#### Why in General
### Big Mud of Ball
@ul
- Messy Code
- Spaghetti Code
- Technical Debt
- ...
- Software can die
    - Out of business (profitable software)
    - No way to update (in-house software)
@ulend

+++
### Big Mud of Ball
#### Productivity vs. Time
![Productivity vs. time](assets/img/Productivity-Time.jpg)

+++
### Is this your engineer life cycle?
![Clean Slate](assets/img/Clean-Slate.jpeg)
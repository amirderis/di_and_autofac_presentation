## Autofac
>Autofac is an addictive Inversion of Control container for .NET Core, ASP.NET Core, .NET 4.5.1+, Universal Windows apps, and more.

+++?image=assets/img/autofac_logo.svg&size=cover&opacity=20
@snap[north span-100]
@css[bullet_header](Autofac)
@ul
- Fairly comprehensive DI Container
- Carefully designed and consistent API
- Available since late 2007
- One of the most popular containers, based on Nuget package download
@ulend

+++

@snap[north span-100]
@css[bullet_header](Simple use of Autofac)
![Autofac simple usage example](assets/img/autofac_simple_example.png)
```
    var builder = new ContainerBuilder();
    builder.RegisterType<Service>();
    IContainer container = builder.Build();
    using(ILifetimeScope scope = container.BeginLifetimeScope()){
        var service = scope.Resolve<Service>();
    }
```
@snapend

+++
@snap[north span-100]

@css[bullet_header](Constructor Injection)
<p>supplying volatile dependency to class constructor as interfaces</p>
```
    Class Foo {
        readonly IDependency D;
        
        public Foo(IDependency d){
            if (d == null) throw new Exception();
            D = d;
        }
    }
```

@snapend

+++

@snap[north span-100]
@css[bullet_header](Mapping abstractions to concrete types)
```
    class Service : IService {

    }

    var builder = new ContainerBuilder();
    builder.RegisterType<Service>()
           .As<IService>();
    IContainer container = builder.Build();
    ILifetimeScope scope = container.BeginLifetimeScope();
    var service = scope.Resolve<IService>();
```

@snapend

+++

@snap[north span-100]
@css[bullet_header](Container configuration options)
![Container configuration options](assets/img/configuration_options.png)
@snapend

Note:
Spring => Config files
Spring Boot => Configuration as Code
+++

@snap[north span-100]
@css[bullet_header](Container configuration: Auto registration)
```
Assembly ingredientsAssembly = typeof(Steak).Assembly;
builder.RegisterAssemblyTypes(ingredientsAssembly)
    .Where(type => type.Name.StartsWith("Sauce"))
    .As<IIngredient>();
```

@css[bullet_header](Container configuration: Decorators)
```
builder.RegisterType<Decoratee>().As<IDecorated>();

builder.RegisterDecorator<InnerDecorator,IDecorated>();

builder.RegisterDecorator<OuterDecorator,IDecorated>();
```
<p>=</p>
```
new OuterDecorator(
    new InnerDecorator(
        new Decoratee()));
```
@snapend

+++

@snap[north span-100]
@css[bullet_header](Lifetime scope)
![Container configuration options](assets/img/lifetime_scope.png)
@snapend

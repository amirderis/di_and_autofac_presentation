### Dependency Injection Details
set of software design principles and patterns that enables developing loosely coupled code
##### Loose Coupling => Extensible Code => Maintainable code

+++
@snap[north span-100]
Dependency Injection Details
@ul
- @css[bullet_header](Late binding): Services can be swapped with other services without recompiling code.
- @css[bullet_header](Extensibility): Code can be extended and reused in ways not explicitly planned for.
- @css[bullet_header](Parallel development): Code can be developed in parallel.
- @css[bullet_header](Maintainability): Classes with clearly defined responsibilities are easier to maintain.
- @css[bullet_header](Testability): Classes can be unit tested.
@ulend
@snapend

+++
<h4>@color[#64bf3d](DI) removes a class’s control over its Dependencies (creation or distruction)</h4> 
&nbsp;
<h4>@color[#64bf3d](DI) enables Interception (Decorator Design Pattern) and Aspect-Oriented Programming for Cross-Cutting Concerns such as Logging, Auditing, ...</h4>
&nbsp;
<h4>@color[#64bf3d](DI) is a means to an end, not a goal in itself.</h4>


+++
@snap[north span-100]
<h3>DI anti-patterns</h3>
@css[bullet_header](The control freak)
<p>Depending on volatile dependency outside 
composition root => tightly couple code.</p>
```
private readonly IProductRepository repository;

public ProductService() {
    this.repository = new SqlProductRepository();
}
```
@snapend

+++
@snap[north span-100]
<h3>DI anti-patterns</h3>
@css[bullet_header](Service Locator)
<p>Asking for granular services from outside Composition Root => hidden dependencies => harder to use and test</p>
```
public class HomeController : Controller {
    public HomeController() { }

    public ViewResult Index()
    {
        IProductService service =
            Locator.GetService<IProductService>();

        var products = service.GetFeaturedProducts();

        return this.View(products);
    }
}
```
@snapend

+++
<h6>Composition Root</h6> <p>we should compose object graphs as close as possible to the application's entry point</p>
![Composition Root](assets/img/composition_root.png)
<p>If we use a DI container, the composition root is the place to use it. Otherwise it leads to @color[#fc4903](Service Locator) anti-pattern</p>

+++

@snap[north span-100]
<h3>DI anti-patterns</h3>
@css[bullet_header](Service Locator)
<h4>Main problems</h4>
@ul
- The class drags along the service locator as a redundant dependency.
- The class makes it non-obvious what its dependencies are.
@ulend

@snapend
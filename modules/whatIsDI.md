### What is ~~Dependency Injection~~ really
#### Dependency Inversion Principle - D in the SOLID
- High-level modules should not depend on low-level modules. Both should depend on abstractions.
- Abstractions should not depend upon details. Details should depend upon abstractions.

Note:
[Dependency Injection Is NOT The Dependency Inversion Principle](https://lostechies.com/derickbailey/2011/09/22/dependency-injection-is-not-the-same-as-the-dependency-inversion-principle/)

+++
### Examples in SyncTool
- High-level/Abstraction: Import/Export Process
- Low-level/Detail: Save/Read Each ERPs

+++
### How - Dependency Injection
@ul
- Separate Constructing a system/services from using it
- Dependency Injection: to inject created services into the using class
- Popular Frameworks
    - Java: Spring (Sprint Boot)
    - .Net: Autofac (Castle Windsor,  Ninject, Linfu, StructureMap, ...)
@ulend
### Evolution of Architecture
#### Traditional Layered Architecture
![Layered Architecture](assets/img/LayeredArchitecture.png)
![Layered Architecture](assets/img/Layers-Flexible.png)

Note:
Infrastructure Dependency(typcial database)
Duplicated cross layers


+++
#### Onion Architecture - layer but full circle
![Onion Architecture](assets/img/Onion.png)

Note:
- The Onion Architecture is layering, but in a full circle!
- The Onion Architecture relies heavily on the Dependency Inversion principle.  The application core needs implementation of core interfaces, and if those implementing classes reside at the edges of the application, we need some mechanism for injecting that code at runtime so the application can do something useful.
- The database is not the center.  It is external.  
+++
### Dependency Inversion Principle (again)
- High-level modules should not depend on low-level modules. Both should depend on abstractions.
- Abstractions should not depend upon details. Details should depend upon abstractions.

+++
#### Onion Architecture-RunTime
![Onion Architecture RunTime](assets/img/OnionArrow.png)

+++?image=assets/img/OnionDesignDetail.png&size=50%
@snap[north span-100]
<h3>Onion design detail</h3>
@snapend

+++
#### Full History of Architecture
https://herbertograca.com/2017/07/03/the-software-architecture-chronicles/
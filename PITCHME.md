@snap[midpoint]
# Depenency Injection And More
@snapend

@snap[south]
### Amir & Hao
@snapend

---

## Agenda
@ul
- Why DI
- What is DI
- Detail
- Autofac
- Evolution of Architecture
@ulend

---?include=modules/whyDI.md
---?include=modules/whatIsDI.md
---?include=modules/DIDetail.md
---?include=modules/autofac.md
---?include=modules/benefitarchitecture.md

---
## The End
### Questions